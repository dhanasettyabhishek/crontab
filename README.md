# Crontab UI

```
A crontab file contains instructions for the cron(8) daemon in the following simplified manner: "run this command at this time on this date".
```

The link to the application is: [https://dhanasettyabhishek.gitlab.io/cron/](https://dhanasettyabhishek.gitlab.io/cron/)

## Tech Stack used:

List of packages and libraries used are:

1. [NodeJS](https://nodejs.org/en/docs/)
2. [VueJS](https://vuejs.org/v2/guide/)
3. [BootstrapVue](https://bootstrap-vue.js.org/docs/)

# Setting up the project locally.

## Clone this project

    Using github, you can clone the document, using the command:
    git clone git@gitlab.com:dhanasettyabhishek/crontab.git

## Project setup

```
To download all the dependencies using nodejs, follow the commands listed:
>npm install
```

### Compiles and hot-reloads for development

```
>npm run serve
```

### Compiles and minifies for production

```
>npm run build
```

### Lints and fixes files

```
>npm run lint
```

### Running the application:

I have already deployed the project on gitlab, and link to the project is:
[Crontab UI](https://dhanasettyabhishek.gitlab.io/cron/)
